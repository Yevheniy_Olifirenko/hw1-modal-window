import React, { useState } from 'react';
import Button from './components/Button/Button';
import ModalImage from './components/Modal/ModalImage';
import ModalText from './components/Modal/ModalText';
import productImage from './components/images/picture.jpg';

const App = () => {
  const [isImageModalOpen, setIsImageModalOpen] = useState(false);
  const [isTextModalOpen, setIsTextModalOpen] = useState(false);

  return (
    <div>
      <Button onClick={() => setIsImageModalOpen(true)}>Open Image Modal</Button>
      <Button onClick={() => setIsTextModalOpen(true)}>Open Text Modal</Button>

      <ModalImage
        isOpen={isImageModalOpen}
        onClose={() => setIsImageModalOpen(false)}
        imageUrl={productImage}
        title="Product Delete!"
        content="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
        confirmText="Confirm"
      />

      <ModalText
        isOpen={isTextModalOpen}
        onClose={() => setIsTextModalOpen(false)}
        title="Add Product “NAME”"
        content="Description for you product"
      />
    </div>
  );
};

export default App;