import React from 'react';
import Modal from './Modal';
import ModalFooter from './ModalFooter';

const ModalText = ({ isOpen, onClose, title, content, confirmText }) => {
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
        <div>
            <h2>{title}</h2>
            <p>{content}</p>
                <ModalFooter
                    firstText="Cancel"
                    firstClick={onClose}
                />
        </div>
        </Modal>
    );
};

export default ModalText;
